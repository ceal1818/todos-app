const App = require("./src/app");

const httpPort = process.env.PORT || 8080;
const mongoHost = process.env.MONGO_HOST || "localhost";

let app = new App(httpPort, mongoHost);
app.start();