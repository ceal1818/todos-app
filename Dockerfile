FROM node:10

RUN mkdir /app && mkdir /app/src

WORKDIR /app

COPY src/ /app/src
COPY index.js /app
COPY package.json /app

CMD npm start