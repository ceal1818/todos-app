const swaggerUi = require('swagger-ui-express');
const bodyParser = require("body-parser");
const express = require("express");
const mongoose = require("mongoose");


const routers = require("./routers");

class App {

    constructor(httpPort, mongoHost) {
        this.httpPort = httpPort;
        this.mongoHost = mongoHost;
    }

    start() {
        let swaggerDocument = require('./swagger.json');

        mongoose.connection.on("connected", event => {
            console.log(`[APP] Database connected: ${this.mongoHost}/todos`);
        });
        
        mongoose.connection.on("error", event => {
            console.log("[ERR] Database error: ", event);
        });
        
        mongoose.connect(
            `mongodb://${this.mongoHost}/todos`, 
            { 
                useNewUrlParser: true, 
                useUnifiedTopology: true
            });
        
        const app = express();
        
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
        app.use(routers.todoRouter);
        
        app.use('/api-docs', swaggerUi.serve, 
            swaggerUi.setup(swaggerDocument));        

        app.listen(this.httpPort, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log("[APP] The application is working.");
            }
        });
    }

}

module.exports = App;