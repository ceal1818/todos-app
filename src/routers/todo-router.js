const express = require("express");
const todoRepository = require("../repositories").todoRepository;

const router = express.Router();
const TODO_PATH = "/todos";

router.post(TODO_PATH, (req, res) => {
    let todo = req.body;
    todoRepository.add(todo, (err) => {
        if (err){
            console.log(err);
        } else {
            res.status(201)
                .json(todo);
        }
    });
});

router.get(TODO_PATH, (req, res) => {
    todoRepository.fetchAll()
        .then(rows => {
            let todos = new Array();
            for (let i = 0; i < rows.length; i++) {
                todos.push({
                    id: rows[i].id,
                    title: rows[i].title,
                    content: rows[i].content,
                    createDate: rows[i].createDate,
                    modifyDate: rows[i].modifyDate
                });
            }
            res.status(200)
                .json(todos);
        })
        .catch(err => {
            res.status(401)
                .json({
                    "message": err
                });
        });
});

module.exports = router;