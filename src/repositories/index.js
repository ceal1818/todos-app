const TodoRepository = require("./todo-repository");

module.exports = {
    todoRepository: new TodoRepository()
};