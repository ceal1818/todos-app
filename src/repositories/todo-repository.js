const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
    title: { type: String, default: "New Todo." },
    content: { type: String, default: "New Todo."},
    createDate: { type: Date, default: Date.now },
    modifyDate: { type: Date }
});
const TodoModel = mongoose.model("todo", TodoSchema);

class TodoRepository {

    add(todo, callback) {
        const instance = new TodoModel();
        instance.title = todo.title;
        instance.content = todo.content;
        instance.createDate = new Date();
        instance.save(err => callback(err));
    }

    fetchAll() {
        return new Promise((resolve, reject) => {
            TodoModel.find({}, (err, rows) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            }); 
        });
    }

}

module.exports = TodoRepository; 